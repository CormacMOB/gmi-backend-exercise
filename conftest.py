import os
import django
import pytest
from django.conf import settings

def pytest_configure():
    settings.DEBUG = False
    django.setup()

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'webapp.settings')

