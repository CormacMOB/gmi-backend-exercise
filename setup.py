"""Setup the project"""
from setuptools import setup, find_packages

setup(name="gmiparse", 
      version="1.0",
      description="Coding exercise for GMI Software Engineer Interview",
      author="Cormac O'Brien",
      author_email="cormacm.obrien@gmail.com",
      packages=find_packages(),
      scripts=['manage.py'])
