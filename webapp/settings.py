"""Minimal settings for the Django app"""
import os

DEBUG = os.environ.get('DEBUG', True)
SECRET_KEY = os.environ.get('SECRET_KEY', os.urandom(32))
ALLOWED_HOSTS = os.environ.get('ALLOWED_HOSTS', 'localhost').split(',')
APP_PATH = os.path.realpath(os.path.dirname(__file__))

DEBUG=DEBUG
SECRET_KEY=SECRET_KEY
ALLOWED_HOSTS=ALLOWED_HOSTS
ROOT_URLCONF='webapp.views'
TEMPLATES = [
    {
    'BACKEND': 'django.template.backends.django.DjangoTemplates',
    'DIRS': [APP_PATH + '/templates/'],
    'APP_DIRS': True,
    }
]
MIDDLEWARE=[
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]
INSTALLED_APPS=(
    'django.contrib.staticfiles',
    'webapp',
)
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(APP_PATH, 'db.ResultEntries')
    }
}

STATIC_URL = '/static/'
