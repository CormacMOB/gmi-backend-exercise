"""Custom Validators for File Import form"""
import os
from lxml import etree
from lxml.etree import XMLSyntaxError
from cStringIO import StringIO
from django.core.exceptions import ValidationError

HOSPITAL_DTD = """<!ELEMENT samples (sample)+>
                  <!ELEMENT sample (patient-id,barcode)>
                  <!ELEMENT patient-id (#PCDATA)>
                  <!ELEMENT barcode (#PCDATA)>
                  """

def validate_file_extension(file_object, valid_ext):
    """Checks for a single filetype and throws Validation Error"""
    path, ext = os.path.splitext(file_object.name)
    if not ext.lower() == valid_ext:
        message = u"Invalid filetype, expected '%s', recieved '%s'"
        raise ValidationError(message % (valid_ext, ext), code="invalid")

def validate_csv_extension(file_object):
    """Reject any file that is not CSV"""
    validate_file_extension(file_object, '.csv')

def validate_xml_dtd(file_object):
    """Check file against DTD"""
    dtd = etree.DTD(StringIO(HOSPITAL_DTD))
    try:
        samples = etree.XML(file_object.read())
    except XMLSyntaxError, e:
        raise ValidationError("XML Syntax was invalid", code="invalid-syntax")
    file_object.seek(0)
    if not dtd.validate(samples):
        raise ValidationError("XML DTD Validation failed", code="invalid-dtd")

def validate_xml_extension(file_object):
    """Reject any file that is not XML"""
    validate_file_extension(file_object, '.xml')
