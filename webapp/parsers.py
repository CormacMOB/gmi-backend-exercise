"""Parser classes and utilities for creating records from imported files"""
import string
import csv
from lxml import etree as ET


class ParserBase(object):
    """Base class for the file parsers"""

    def fetch_records(self):
        """Process records"""
        return [self.create_record(row) for row in self.reader]


class XMLParser(ParserBase):
    """Parse incoming XML file"""
    def __init__(self, xml_file):
        xml_data = xml_file.read()
        self.reader = (sample for sample in ET.fromstring(xml_data))

    def create_record(self, sample):
        """Validate and process sample record"""
        sample_record = {'patient_id':sample.findtext('patient-id'),
                         'barcode':sample.findtext('barcode')}
        return sample_record

class CSVParser(ParserBase):
    """Parse incoming CSV File of DNA tests"""
    @staticmethod
    def slot_to_grid_position(slot):
        """
            Convert an integer in range 1 - 96 to a grid position
        """
        if slot <= 0 or slot > 96:
            raise ValueError("Slot value out of range")
        column = slot % 12
        letter_index = (slot/12)
        if column == 0:
            # multiples of twelve correspond to the last entry in a row
            column = 12
            row = string.ascii_uppercase[(letter_index - 1)]
        else:
            row = string.ascii_uppercase[letter_index]
        return '%s:%d' % (row, column)

    def __init__(self, csv_file):
        """Setup csv file reader and skip the header if necessary"""
        has_header = csv.Sniffer().has_header(csv_file.read(1024))
        csv_file.seek(0)
        self.reader = (row for row in csv.reader(csv_file, delimiter=',', quotechar=' '))
        if has_header:
            # Skip the header
            next(self.reader)

    def create_record(self, row):
        """Create record with converted well positon"""
        return {'patient_id': row[0],
                'two_d_barcode': row[1],
                'well_position': self.slot_to_grid_position(int(row[2]))}
