"""Model logic for the application.
    merge_dicts and merge_records are utility functions only used here"""
from django.db import models
from webapp.parsers import CSVParser, XMLParser

def merge_dicts(dict1, dict2):
    """Returns a merged dict for use in list comprehensions over zip"""
    dict1.update(dict2)
    return dict1

def merge_records(collection1, collection2):
    """Merge two lists of dicts in order and return as an iterator"""
    record_list = (merge_dicts(*pair) for pair in zip(collection1, collection2))
    return record_list


class ResultEntryManager(models.Manager):
    """Custom manager to allow bulk loading from files"""
    def merge_records(self, hospital_records, dna_records):
        """Merge the two record sources in one iterator"""
        record_list = (merge_dicts(*pair) for pair in zip(hospital_records, dna_records))
        return record_list

    def instantiate_model_object(self, record):
        """Create a model instance"""
        return ResultEntry(
            patient_id=record['patient_id'],
            two_d_barcode=record['two_d_barcode'],
            barcode=record['barcode'],
            position=record['well_position'])

    def fetch_model_list(self, hospital_records, dna_records):
        """Pre instantiate model objects for batch_creation"""
        record_list = merge_records(hospital_records, dna_records)
        model_objects = []
        patient_ids = []
        for record in record_list:
            model_objects.append(self.instantiate_model_object(record))
            patient_ids.append(record['patient_id'])
        return model_objects, patient_ids

    def create_from_files(self, xml_file=None, csv_file=None):
        """Parses the two files, Merges the two files and bulk creates them"""
        hospital_records = XMLParser(xml_file).fetch_records()
        dna_records = CSVParser(csv_file).fetch_records()
        models_to_create, new_patient_ids = self.fetch_model_list(hospital_records, dna_records)
        ResultEntry.objects.bulk_create(models_to_create)
        return new_patient_ids


class ResultEntry(models.Model):
    """Model for storing the combined test results
            patient_id: The Patient ID from both inputs
            barcode: The flexstar barcode
            two_d_barcode: The 2d Barcode from the hospital import
            position: Well position in the Alpha numeric form described. e.g. 1 = A:1
    """
    patient_id = models.CharField(max_length=10, primary_key=True)
    barcode = models.CharField(max_length=10)
    two_d_barcode = models.CharField(max_length=10)
    position = models.CharField(max_length=4)

    objects = ResultEntryManager()
