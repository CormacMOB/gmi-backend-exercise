"""API for the gmiparse application. For berevity sake, urls views 
    and forms are all stored here"""
from django import forms
from django.urls import reverse
from django.utils.http import urlencode
from django.shortcuts import render, redirect
from django.http import HttpResponseNotAllowed
from django.conf.urls import url
from django.views.generic import ListView
from webapp.models import ResultEntry
from webapp.validators import validate_xml_extension, validate_csv_extension, validate_xml_dtd

class FileUploadForm(forms.Form):
    """Upload form for validation of files"""
    xml_file = forms.FileField(validators=[validate_xml_extension, validate_xml_dtd])
    csv_file = forms.FileField(validators=[validate_csv_extension])

class SampleSearchForm(forms.ModelForm):
    """Form for managing the search form"""
    class Meta:
        model = ResultEntry
        fields = ['patient_id', 'barcode', 'two_d_barcode', 'position']

class ResultsView(ListView):
    """Generic View for listing DNA results"""
    model = ResultEntry
    template_name = 'result_list.html'

    def get_queryset(self):
        """ Filter Queryset by the given parameters"""
        query = self.request.GET
        query_set = ResultEntry.objects.all()
        if query.get('patient_ids'):
            # Display after upload
            query_set.filter(patient_id__in=query.get('patient_ids'))
        else:
            # Search
            if query.get('patient_id'):
                query_set = query_set.filter(patient_id__icontains=query['patient_id'])
            if query.get('barcode'):
                query_set = query_set.filter(barcode__icontains=query['barcode'])
            if query.get("two_d_barcode"):
                query_set = query_set.filter(two_d_barcode__icontains=query['two_d_barcode'])
            if query.get("position"):
                query_set = query_set.filter(position__icontains=query['position'])
        return query_set

def upload(request):
    """View for upload and processing of data files"""
    if request.method == 'GET':
        form = FileUploadForm()
    elif request.method == 'POST':
        files = request.FILES
        form = FileUploadForm(request.POST, files)
        if form.is_valid():
            new_patient_ids = ResultEntry.objects.create_from_files(xml_file=files['xml_file'],
                    csv_file=files['csv_file'])
            redirect_url = reverse('sample-results')
            # reedirect with a list of patient ids to the results view.
            query_string = urlencode({'patient_ids': new_patient_ids})
            url = '%s?%s' %(redirect_url, query_string)
            return redirect(url)
        else:
            return render(request, "upload.html", {'form': form}, status=400)
    return render(request, "upload.html", {'form': form})

def search(request):
    """Search for a Sample Record"""
    if request.method != 'GET':
        return HttpResponseNotAllowed()
    else:
        form = SampleSearchForm()
    return render(request, "search.html", {'form': form})

urlpatterns = (
    url(r'^$', upload),
    url(r'upload', upload),
    url(r'search_form', search),
    url(r'results', ResultsView.as_view(), name="sample-results"),
)
