import pytest
from django.core.exceptions import ValidationError
from webapp.validators import validate_file_extension, validate_xml_extension, validate_xml_dtd


@pytest.fixture()
def xml_file():
    xml_file = open("data/hospital-data-input.xml")
    yield xml_file
    xml_file.close()

@pytest.fixture()
def csv_file():
    csv_file = open("data/flexstar-output.csv")
    yield csv_file
    csv_file.close()

@pytest.fixture()
def text_file():
    text_file = open("data/dangerous.txt")
    yield text_file
    text_file.close()


def test_validation_fails_invalid_file_extention(text_file):
    with pytest.raises(ValidationError, 
            match="Invalid filetype, expected '.csv', recieved '.txt'"):
        validate_file_extension(text_file, '.csv')

def test_csv_extention_valdation_valid(csv_file):
    try:
        validate_file_extension(csv_file, '.csv')
    except ValidationError:
        pytest.fail("Unexpected ValidationError. Filetype is csv, but threw error")

def test_xml_filetype_validator(xml_file):
    """Test the xml file specialisation of the validator"""
    try:
        validate_xml_extension(xml_file)
    except ValidationError:
        pytest.fail("Unexpected ValidationError. Filetype is xml, but function rejected it")

def test_xml_dtd_validator(xml_file):
    """Test the xml file specialisation of the validator"""
    try:
        validate_xml_dtd(xml_file)
    except ValidationError:
        pytest.fail("XML DTD Validation failed")

def test_xml_filetype_validator_wrong_type(csv_file):
    """Test the xml file specialisation of the validator"""
    with pytest.raises(ValidationError, 
            match="Invalid filetype, expected '.xml', recieved '.csv'"):
        validate_xml_extension(csv_file)
