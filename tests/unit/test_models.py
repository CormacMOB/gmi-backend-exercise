import pytest
from webapp.models import ResultEntryManager

@pytest.fixture()
def xml_file():
    xml_file = open("data/hospital-data-input.xml")
    yield xml_file
    xml_file.close()

@pytest.fixture()
def csv_file():
    csv_file = open("data/flexstar-output.csv")
    yield csv_file
    csv_file.close()

