"""Test suite for the CSV and XML parsers"""
from cStringIO import StringIO
from xml.etree import ElementTree as ET
import pytest
from webapp.parsers import XMLParser, CSVParser

def test_slot_to_grid_position():
    """Test the correct values"""
    slot_to_grid = CSVParser.slot_to_grid_position
    assert slot_to_grid(1) == 'A:1'
    assert slot_to_grid(13) == 'B:1'
    assert slot_to_grid(12) == 'A:12'
    assert slot_to_grid(96) == 'H:12'

def test_slot_to_grid_out_of_range():
    """The function should raise a ValueError when given an out
    of range slot number"""
    slot_to_grid = CSVParser.slot_to_grid_position
    with pytest.raises(ValueError, match="Slot value out of range"):
        slot_to_grid(0)
    with pytest.raises(ValueError, match="Slot value out of range"):
        slot_to_grid(97)


@pytest.fixture()
def xml_data():
    """Import sample data xml file for testing"""
    data = open('data/hospital-data-input.xml')
    yield data
    data.close()

def test_xml_parser_instantiation(xml_data):
    """Test that the parser instantiates"""
    assert XMLParser(xml_data) is not None

def test_record_parse(xml_data):
    """Test that the first record gets parsed"""
    parser = XMLParser(xml_data)
    sample = parser.fetch_records()[0]
    assert sample['patient_id'] == 'ID001'

def test_all_records_parsed(xml_data):
    parser = XMLParser(xml_data)
    assert len(parser.fetch_records()) == 96

@pytest.fixture()
def csv_data():
    """Import the csv"""
    data = open('data/flexstar-output.csv')
    yield data
    data.close()

def test_csv_parser_instantiation(csv_data):
    assert CSVParser(csv_data) is not None

def test_csv_parser_good_output(csv_data):
    records = CSVParser(csv_data).fetch_records()
    sample, sample2 = records[0], records[-1]
    assert sample['patient_id'] == 'ID001'
    assert sample['well_position'] == 'A:1'
    assert sample2['well_position'] == 'H:12'

def test_all_records_parsed(csv_data):
    parser = CSVParser(csv_data)
    assert len(parser.fetch_records()) == 96

