"""Tests for view function calls"""
import os
import tempfile
from lxml import html
import pytest
from django.test import Client, TestCase

@pytest.fixture()
def test_app():
    test_app = Client()
    test_app.testing = True
    return test_app

@pytest.fixture()
def xml_file():
    xml_file = open("data/hospital-data-input.xml")
    yield xml_file
    xml_file.close()

@pytest.fixture()
def csv_file():
    csv_file = open("data/flexstar-output.csv")
    yield csv_file
    csv_file.close()

def count_results(doc):
    """Count the rows in the result table"""
    return len(doc.find_class('result-row'))

def test_home_view_exists(test_app):
    result = test_app.get('/')
    assert result.status_code == 200

def test_html_in_response(test_app):
    response = test_app.get('/')
    assert '<html>' in response.content 

def test_no_files(test_app):
    response = test_app.post('/upload')
    assert response.status_code == 400

def test_sample_search(test_app):
    response = test_app.get('/search_form')
    assert response.status_code == 200


class TestUpload(TestCase):
    """Test behavior of upload"""
    def setUp(self):
        self.test_app = Client()


    @pytest.mark.django_db
    def test_file_upload_rejects_bad_csv_file(self):
        """The upload should fail if it gets a file that is not csv or xml"""
        with open('data/dangerous.txt') as csv_file, open('data/hospital-data-input.xml') as xml_file:
            response = self.test_app.post('/upload', {'csv_file':csv_file, 'xml_file': xml_file}, follow=True)
            assert response.status_code == 400

    @pytest.mark.django_db
    def test_file_upload_rejects_bad_xml_file(self):
        """The upload should fail if it gets an xml file that is invalid (xml injection)"""
        with open('data/flexstar-output.csv') as csv_file, open('data/dangerous.xml') as xml_file:
            response = self.test_app.post('/upload', {'csv_file':csv_file, 'xml_file': xml_file}, follow=False)
            assert response.status_code == 400

    @pytest.mark.django_db
    def test_file_upload_rejects_invalid_xml_file(self):
        """The upload should fail if it gets a file that fails DTD validation"""
        with open('data/flexstar-output.csv') as csv_file, open('data/invalid.xml') as xml_file:
            response = self.test_app.post('/upload', {'csv_file':csv_file, 'xml_file': xml_file}, follow=False)
            assert response.status_code == 400

    @pytest.mark.django_db
    def test_file_upload_redirects(self):
        """A successful upload should redirect"""
        with open('data/flexstar-output.csv') as csv_file, open('data/hospital-data-input.xml') as xml_file:
            response = self.test_app.post('/upload', {'csv_file':csv_file, 'xml_file': xml_file}, follow=False)
            assert response.status_code == 302

    @pytest.mark.django_db
    def test_file_upload_follows(self):
        """A successful upload should display all records"""
        with open('data/flexstar-output.csv') as csv_file, open('data/hospital-data-input.xml') as xml_file:
            response = self.test_app.post('/upload', {'csv_file':csv_file, 'xml_file': xml_file}, follow=True)
            doc = html.fromstring(response.content)
            results = count_results(doc)
            assert response.status_code == 200
            assert results == 96

class TestSearch(TestCase):
    """Test that the search works"""
    def setUp(self):
        """Load the database"""
        self.test_app = Client()
        with open('data/flexstar-output.csv') as csv_file, open('data/hospital-data-input.xml') as xml_file:
            response = self.test_app.post('/upload', {'csv_file':csv_file, 'xml_file': xml_file}, follow=False)

    @pytest.mark.django_db
    def test_search_finds_record(self):
        response = self.test_app.get('/results?patient_id=ID001')
        doc = html.fromstring(response.content)
        results = count_results(doc)
        assert response.status_code == 200
        assert results == 11

